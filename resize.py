import cv2
import numpy as np
import os, argparse
from data import process_image_file
from lucid_utils import read_results_file


if __name__ == '__main__':

    results = read_results_file()
    diseases = ['normal', 'pneumonia', 'COVID-19']
    
    for entry in results.keys():
        img_name = results[entry]['image_name']
        print(img_name)

        #load image and save in new thing
        for disease in diseases:
            img = process_image_file('output/gradcam/no_crop_old/' + disease + '/' + img_name, 0.0, 480)
            cv2.imwrite('output/gradcam/no_crop/' + disease + '/' + img_name, img)
        
        